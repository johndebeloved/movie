SQL Insert



insert into movies(movie_id,name,genre,is_favorite,released_date,created_at)
values (1,'Aladdin','Fantasy',true,current_timestamp,current_timestamp),
(2,'Snow White','Fantasy',false,current_timestamp,current_timestamp),
(3,'The Grudge','Horror',false,current_timestamp,current_timestamp),
(4,'Saving Private Ryan','War',true,current_timestamp,current_timestamp),
(5,'Killing me softly','Romance',false,current_timestamp,current_timestamp),
(6,'Pearl Harbor','War',true,current_timestamp,current_timestamp),
(7,'The Hangover','Comedy',false,current_timestamp,current_timestamp),
(8,'The Hangover 2','Comedy',true,current_timestamp,current_timestamp),
(9,'The Hangover 3','Comedy',true,current_timestamp,current_timestamp),
(10,'Conjuring','Horror',false,current_timestamp,current_timestamp);

POSTGRESQL
Spring Data JPA
Spring Web
Lombok