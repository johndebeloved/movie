package com.example.demo.processor;

import java.util.HashMap;
import java.util.Map;

import com.example.demo.commonresponse.Response;
import com.example.demo.constant.StatusCode;
import com.example.demo.param.BaseParams;

public interface IMovieProcessor<T extends BaseParams> {

	public Response process(T t);

	default Response generateSuccessResponse(Object object) {
		return createOkResponse(StatusCode.SUCCESS, object);
	}

	default Response generateSuccessResponse(StatusCode statusCodes) {
		return createOkResponse(statusCodes);
	}

	private Response createOkResponse(StatusCode statusCodes, Object object) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", object);
		return new Response(statusCodes.getStatusCode(), map);
	}

	private Response createOkResponse(StatusCode statusCodes) {
		return new Response(statusCodes.getStatusCode());
	}

	default Response generateErrorResponse(StatusCode statusCodes) {
		return createErrorResponse(statusCodes, null);
	}

	private Response createErrorResponse(StatusCode statusCodes, Object object) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", statusCodes.getHttpStatus());
		return new Response(map);
	}
}
