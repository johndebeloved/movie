package com.example.demo.processor.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.commonresponse.Response;
import com.example.demo.dto.MovieDto;
import com.example.demo.param.BaseParams;
import com.example.demo.processor.IMovieProcessor;
import com.example.demo.repository.IMovieRepository;
import com.example.demo.response.GetMoviesResponse;

@Component
public class GetAllFavoriteMovie implements IMovieProcessor<BaseParams> {
	
	@Autowired
	private IMovieRepository iMovieRepository;

	@Override
	public Response process(BaseParams t) {
		
		List<MovieDto> movies = iMovieRepository.findAllIsFavorite().stream().map((movie) -> MovieDto.build(movie)).collect(Collectors.toList());
		
		GetMoviesResponse response = new GetMoviesResponse();
		response.setTotalSize(movies.size());
		response.setMovies(movies);
		
		return generateSuccessResponse(response);
	}

}
