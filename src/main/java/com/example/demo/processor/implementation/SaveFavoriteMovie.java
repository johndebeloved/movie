package com.example.demo.processor.implementation;

import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.commonresponse.Response;
import com.example.demo.constant.StatusCode;
import com.example.demo.entity.Movie;
import com.example.demo.param.SaveFavoriteParams;
import com.example.demo.processor.IMovieProcessor;
import com.example.demo.repository.IMovieRepository;

@Component
public class SaveFavoriteMovie implements IMovieProcessor<SaveFavoriteParams> {
	
	@Autowired
	private IMovieRepository iMovieRepository;

	@Override
	public Response process(SaveFavoriteParams t) {
		
		Movie movie = iMovieRepository.getById(t.getMovieId());
		
		if(Objects.isNull(movie)) {
			return generateErrorResponse(StatusCode.DOES_NOT_EXIST);	
		}
		
		saveMovie(movie);

		return generateSuccessResponse(StatusCode.SUCCESS);
	}
	
	private Movie saveMovie(Movie movie) {
		Date now = new Date();
		movie.setIsFavorite(true);
		movie.setUpdatedAt(now);
		return iMovieRepository.save(movie);
	}

}
