package com.example.demo.processor.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.commonresponse.Response;
import com.example.demo.dto.MovieDto;
import com.example.demo.param.GetMovieByNameOrGenreParams;
import com.example.demo.processor.IMovieProcessor;
import com.example.demo.repository.IMovieRepository;
import com.example.demo.response.GetMoviesResponse;

@Component
public class GetMoviesByNameOrGenre implements IMovieProcessor<GetMovieByNameOrGenreParams> {
	
	@Autowired
	private IMovieRepository iMovieRepository;

	@Override
	public Response process(GetMovieByNameOrGenreParams params) {
		
		List<MovieDto> movies = iMovieRepository.findByNameOrGender(params.getSearch()).stream().map(MovieDto::build).collect(Collectors.toList());
		GetMoviesResponse response = new GetMoviesResponse();
		response.setMovies(movies);
		response.setSize(movies.size());
		return generateSuccessResponse(response);

	}
}