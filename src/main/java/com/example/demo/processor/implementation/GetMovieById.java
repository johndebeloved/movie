package com.example.demo.processor.implementation;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.commonresponse.Response;
import com.example.demo.constant.StatusCode;
import com.example.demo.dto.MovieDto;
import com.example.demo.param.GetMovieParams;
import com.example.demo.processor.IMovieProcessor;
import com.example.demo.repository.IMovieRepository;

@Component
public class GetMovieById implements IMovieProcessor<GetMovieParams> {
	
	@Autowired
	private IMovieRepository iMovieRepository;

	@Override
	public Response process(GetMovieParams params) {
		
		MovieDto movie = MovieDto.build(iMovieRepository.getById(params.getMovieId()));
		if(Objects.isNull(movie)) {
			return generateErrorResponse(StatusCode.DOES_NOT_EXIST);
		}
		
		return generateSuccessResponse(movie);
	}

}
