package com.example.demo.param;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class GetMovieByNameOrGenreParams extends BaseParams {

	  private @Getter @Setter String search;
}
