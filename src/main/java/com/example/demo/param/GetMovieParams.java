package com.example.demo.param;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Data
@EqualsAndHashCode(callSuper=false)
public class GetMovieParams extends BaseParams {
	
	  @ApiParam(hidden = true)
	  private @Getter @Setter Long movieId;
}