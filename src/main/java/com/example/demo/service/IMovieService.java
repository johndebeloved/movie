package com.example.demo.service;

import org.springframework.stereotype.Component;

import com.example.demo.commonresponse.Response;
import com.example.demo.param.BaseParams;
import com.example.demo.param.GetMovieByNameOrGenreParams;
import com.example.demo.param.GetMovieParams;
import com.example.demo.param.SaveFavoriteParams;

@Component
public interface IMovieService {

	Response getAllFavoriteMovies(BaseParams params);
	
	Response saveFavoriteMovie(SaveFavoriteParams params);
	
	Response getMoviesByNameOrGenre(GetMovieByNameOrGenreParams params);
	
	Response getMovieById(GetMovieParams params);
}
