package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.commonresponse.Response;
import com.example.demo.param.BaseParams;
import com.example.demo.param.GetMovieByNameOrGenreParams;
import com.example.demo.param.GetMovieParams;
import com.example.demo.param.SaveFavoriteParams;
import com.example.demo.processor.IMovieProcessor;

@Service
public class MovieService implements IMovieService {
	
	@Autowired
	private IMovieProcessor<BaseParams> getAllFavoriteMovies;
	
	@Autowired
	private IMovieProcessor<SaveFavoriteParams> saveFavoriteMovie; 
	
	@Autowired
	private IMovieProcessor<GetMovieByNameOrGenreParams> getMoviesByNameOrGenre; 
	
	@Autowired
	private IMovieProcessor<GetMovieParams> getMovieById; 
	

	@Override
	public Response getAllFavoriteMovies(BaseParams params) {
		return getAllFavoriteMovies.process(params);
	}

	@Override
	public Response saveFavoriteMovie(SaveFavoriteParams params) {
		return saveFavoriteMovie.process(params);
	}

	@Override
	public Response getMoviesByNameOrGenre(GetMovieByNameOrGenreParams params) {
		return getMoviesByNameOrGenre.process(params);
	}

	@Override
	public Response getMovieById(GetMovieParams params) {
		return getMovieById.process(params);
	}
}
