package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Movie;

@Repository
public interface IMovieRepository extends JpaRepository<Movie, Long> {

	@Query("select m from Movie m where m.isFavorite = true")
	List<Movie> findAllIsFavorite();
	
	@Query("select m from Movie m where lower(m.name) like lower(concat('%', :search,'%')) OR lower(m.genre) like lower(concat('%',:search))")
	List<Movie> findByNameOrGender(String search);
}
