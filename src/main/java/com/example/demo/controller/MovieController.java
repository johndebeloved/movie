package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.commonresponse.Response;
import com.example.demo.param.BaseParams;
import com.example.demo.param.GetMovieByNameOrGenreParams;
import com.example.demo.param.GetMovieParams;
import com.example.demo.param.SaveFavoriteParams;
import com.example.demo.service.IMovieService;

@RestController
public class MovieController extends BaseRestController {
	
	@Autowired
	private IMovieService iMovieService;

	
	//GET favorites/
	@GetMapping("/favorites")
	public ResponseEntity<Response> getAllFavoriteMovies(BaseParams params){
		return createOkResponse(iMovieService.getAllFavoriteMovies(params));
	}
	
	//POST favorites/id
	@PostMapping("/favorites/{movieId}")
	public ResponseEntity<Response> saveFavoriteMovie(SaveFavoriteParams params){
		return createOkResponse(iMovieService.saveFavoriteMovie(params));
	}
	
	//GET movies?search
	@GetMapping("/movies")
	public ResponseEntity<Response> getMoviesByNameOrGenre(@RequestParam(value="search")String search, GetMovieByNameOrGenreParams params){
		params.setSearch(search);
		return createOkResponse(iMovieService.getMoviesByNameOrGenre(params));
	}
	
	//GET movies/id
	@GetMapping("/movies/{movieId}")
	public ResponseEntity<Response> getMovieById(GetMovieParams params){
		return createOkResponse(iMovieService.getMovieById(params));
	}
	
}