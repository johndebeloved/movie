package com.example.demo.controller;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;

import com.example.demo.commonresponse.Response;

@ComponentScan(basePackages = {"com.example.demo"})
public class BaseRestController {

	public ResponseEntity<Response> createOkResponse(Response obj) {
		return ResponseEntity.ok().body(obj);
	}
	
	public ResponseEntity<Response> createOkResponse() {
		return ResponseEntity.ok().build();
	}
}
