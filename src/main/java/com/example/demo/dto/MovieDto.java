package com.example.demo.dto;

import java.util.Date;

import com.example.demo.entity.Movie;

import lombok.Data;

@Data
public class MovieDto {

	private Long id;
	private String name;
	private String genre;
	private Date releasedDate;
	
	
	public static MovieDto build (Movie movie) {
		MovieDto dto = new MovieDto();
		dto.setId(movie.getId());
		dto.setName(movie.getName());
		dto.setGenre(movie.getGenre());
		dto.setReleasedDate(movie.getReleasedDate());
		return dto;
	}
}
