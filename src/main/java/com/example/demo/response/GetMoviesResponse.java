package com.example.demo.response;

import java.util.List;

import com.example.demo.dto.MovieDto;

import lombok.Data;

@Data
public class GetMoviesResponse {

	private int size;
	private int totalSize;
	private List<MovieDto> movies;
}
