package com.example.demo.commonresponse;

import java.util.Map;

import lombok.Data;

@Data
public class Response {
	
	  private int statusCode;
	  private Map<String, Object> body;

	  public Response(int statusCode) {
	    this.statusCode = statusCode;
	  }

	  public Response(Map<String, Object> body) {
	    this.body = body;
	  }

	  public Response(int statusCode, Map<String, Object> body) {
	    this.statusCode = statusCode;
	    this.body = body;
	  }
}
