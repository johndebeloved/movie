package com.example.demo.constant;

import org.springframework.http.HttpStatus;
import lombok.Getter;

@Getter
public enum StatusCode {
	SUCCESS(200, HttpStatus.OK), CREATED(201, HttpStatus.CREATED),
	ERROR_DATABASE_CONNECTION(500, HttpStatus.INTERNAL_SERVER_ERROR), DOES_NOT_EXIST(404, HttpStatus.NOT_FOUND),
	NO_RESULT(404, HttpStatus.NOT_FOUND);

	private int statusCode;
	private HttpStatus httpStatus;

	private StatusCode(int statusCode, HttpStatus httpStatus) {
		this.statusCode = statusCode;
		this.httpStatus = httpStatus;
	}
}