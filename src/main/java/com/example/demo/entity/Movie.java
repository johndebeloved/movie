package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="movies")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Movie {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="MOVIE_ID")
	private Long id;
	@Column(name="NAME")
	private String name;
	@Column(name="GENRE")
	private String genre;
	@Column(name="IS_FAVORITE")
	private Boolean isFavorite;
	@Column(name="RELEASED_DATE")
	private Date releasedDate;
	@Column(name="CREATED_AT")
	private Date createdAt;
	@Column(name="UPDATED_AT")
	private Date updatedAt;
	
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		return builder.append("Movies [").append("id="+id).append("id="+id)
		.append("id="+id).toString();
	}
}
